%define	       debug_package %{nil}

Name:          python-urwid
Version:       2.6.16
Release:       1
Summary:       Console user interface library
License:       LGPLv2+
URL:           http://excess.org/urwid/

Source0:       https://pypi.python.org/packages/source/u/urwid/urwid-%{version}.tar.gz
Patch1:        Add-script-for-detecting-character-widths-and-dumping-width-tables.patch
Patch2:        Remove-test-cov-arguments-to-fix-pytest-unrecognized-arguments.patch

BuildRequires: python3-pip  python3-hatchling python3-hatch-vcs python3-wheel
BuildRequires: python3-wcwidth python3-pytest

%description
Urwid is a console user interface library for Python. It includes
many features useful for text console application developers

%package -n python3-urwid
Summary:       %summary
%{?python_provide:%python_provide python3-urwid}
BuildRequires: python3-devel python3-setuptools python3-test /usr/bin/2to3 gcc glibc-all-langpacks

%description -n python3-urwid
Urwid is a console user interface library for Python. It includes
many features useful for text console application developers

%prep
%autosetup -n urwid-%{version} -p1
find urwid -type f -name "*.py" -exec sed -i -e '/^#!\//, 1d' {} \;
find urwid -type f -name "*.py" -exec chmod 644 {} \;

%build
%pyproject_build
find examples -type f -exec chmod 0644 \{\} \;

%install
%pyproject_install

%check
%pytest tests/

%files -n python3-urwid
%doc README.rst examples docs COPYING
%{python3_sitelib}/urwid*

%changelog
* Tue Dec 10 2024 xu_ping<707078654@qq.com> - 2.6.16-1
- Upgrade version to 2.6.16
  * Support relative scroll for ListBox
  * Fix regression in TreeWidget: original widget can be overridden
  * Do not use deprecated positioning in the code and examples
  * Prevent a possible infinite loop in WidgetDecoration.base_widget 
  * Adopt ExceptionGroup handling without an external library in python 3.11+
  * Disable mouse tracking and discard input when exiting the main loop
  * use "target encoding" while transcoding for output 

* Sat Jul 22 2023 xu_ping<707078654@qq.com> - 2.1.2-5
- fix test failure due to python11

* Tue Jan 17 2023 zhangliangpengkun<zhangliangpengkun@xfusion.com> - 2.1.2-4
- Add-script-for-detecting-character-widths-and-dumping-width-tables.patch

* Fri Jan 13 2023 zhangliangpengkun<zhangliangpengkun@xfusion.com> - 2.1.2-3
- fix-use-trio-lowlevel-instead-of-trio.patch

* Tue Jul 27 2021 liyanan <liyanan32@huawei.com> - 2.1.2-2
- Add buildrequires glibc-all-langpacks to fix build failed

* Tue Jul 27 2021 liyanan <liyanan32@huawei.com> - 2.1.2-1
- update to 2.1.2

* Mon May 31 2021 huanghaitao <huanghaitao8@huawei.com> - 2.3-7
- Completing build dependencies

* Thu Oct 22 2020 zhanghua <zhanghua40@huawei.com> - 2.3-6
- remove python2 subpackage

* Fri Nov 22 2019 sunguoshuai <sunguoshuai@huawei.com> - 2.3-5
- Package init.
